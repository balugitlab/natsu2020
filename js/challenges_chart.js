// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';


// Chart
var challengesCanvas = document.getElementById("challengesChart");

async function createChallengesChart() {
  await fetch(`https://cultris.net:5001/leaderboard_stats_by_id/` + userID)
    .then(response => response.json())
    .then(data => {
      playerChallengeData = data
      var challenges = [];
      var ranks = [];
      var scores = [];
      let all_challenges = ["Maserati", "Fortnight", "Cheese", "Ten", "Survivor"]
      playerChallengeData.forEach( item => {
        challenges.push(item.challengename.charAt(0).toUpperCase() + item.challengename.slice(1));
        ranks.push(item.rank);
        scores.push(item.score)
      });

      if (challenges.length < 5) {
        console.log("User has less than 5 challenges with rank sub 200!")
        all_challenges.forEach(item => {
          if (challenges.indexOf(item) < 0) {
            console.log("Missing challenge " + item)
            challenges.push(item)
            ranks.push(200)
            scores.push(0)
          }

        })
      }

      var challengesData = {
        labels: challenges,
        datasets: [{
          label: playerChallengeData[0].name,
          backgroundColor: "rgba(200,0,0,0.2)",
          data: ranks
        }]
      };

      var options = {
        legend: {
          display: false,
        },
        scale: {
          angleLines: {
            display: true
            },
        maintainAspectRatio: false,
        // responsive: false,
        ticks: {
          suggestedMin: 1,
          suggestedMax: 200,
          reverse: true
        },
      }
    };

      var radarChart = new Chart(challengesCanvas, {
        type: 'radar',
        data: challengesData,
        options: options
      });

    });


}

createChallengesChart()
