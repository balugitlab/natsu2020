function loadTableData(items) {
  const table = document.getElementById("dataTable");
  items.forEach( item => {
    let row = table.insertRow();
    let challenge = row.insertCell(0);
    challenge.innerHTML = item.challengename.charAt(0).toUpperCase() + item.challengename.slice(1);
    let rank = row.insertCell(1);
    rank.innerHTML = item.rank;
    let score = row.insertCell(2);
    score.innerHTML = item.score;
  });
}

function loadplayerData(playerData) {
  document.getElementById("playerName").innerHTML = playerData.name
  document.getElementById("playerRank").innerHTML = playerData.rank + " of " + totalPlayers
  document.getElementById("playerWeekPlayTime").innerHTML = Math.round(playerData.WeekPlaytime / 60) + " Hours"
  document.getElementById("playerNetScore").innerHTML = playerData.netscore + " Points"

  document.getElementById("playerTimePlayed").innerHTML = "Total Hours Played: " + Math.round(playerData.playedmin/60)
  document.getElementById("playerGamesPlayed").innerHTML = "Total Games Played: " + Math.round(playerData.playedrounds)
  document.getElementById("playerWinRate").innerHTML = "Win Rate: " + Math.round(100*(playerData.wins) / (playerData.playedrounds)) + "%"
  document.getElementById("playerAvgBPM").innerHTML = "Average Speed: " + Math.round(playerData.avgbpm) + " BPM"
  document.getElementById("playerMaxBPM").innerHTML = "Max Speed: " + Math.round(playerData.maxbpm) + " BPM"

  document.getElementById("playerTimeStamp").innerHTML = "Last Updated: " + playerData.timestamp.substring(0,10)
}

async function getplayerChallengeData() {
  const response = await fetch('https://cultris.net:5001/leaderboard_stats_by_id/' + userID)
  const data = await response.json()
  return data
}

async function getTotalPlayers() {
  const response = await fetch('https://cultris.net:5000/full_rankings/');
  const data = await response.json();
  return data.length;
}

async function getplayerData() {
  const response = await fetch(`https://cultris.net:5000/current_stats_by_id/` + userID);
  const data = await response.json();
  return data
}
getplayerChallengeData().then(data => {
  playerChallengeData = data;
  loadTableData(playerChallengeData)
});

getplayerData().then(data => {
  playerData = data
  getTotalPlayers().then(data => {
    totalPlayers = data;
    loadplayerData(playerData)
  });
});
