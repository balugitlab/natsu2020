// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';
var challenges_1 = [];
var ranks_1 = [];
var scores_1 = [];
var playerChallengeData_1
var challenges_2 = [];
var ranks_2 = [];
var scores_2 = [];
var playerChallengeData_2
const all_challenges = ["maserati", "fortnight", "cheese", "ten", "survivor"]


var options = {
  legend: {
    display: true,
  },
  scale: {
    angleLines: {
      display: true
      },
  maintainAspectRatio: false,
  // responsive: false,
  ticks: {
    suggestedMin: 1,
    suggestedMax: 200,
    reverse: true
  },
}
};

async function getchartData_1() {
  await fetch(`https://cultris.net:5001/leaderboard_stats_by_id/` + userID_1)
    .then(response => response.json())
    .then(data => {
      playerChallengeData_1 = data
      console.log(playerChallengeData_1)

      all_challenges.forEach( item => {
        let cn = item
        playerChallengeData_1.forEach (entry => {
          if (entry.challengename == cn) {
            challenges_1.splice(all_challenges.indexOf(cn), 0, cn)
            ranks_1.splice(all_challenges.indexOf(cn), 0, entry.rank)
            scores_1.splice(all_challenges.indexOf(cn), 0, entry.score)
          }
        })
      })

      if (challenges_1.length < 5) {
        // console.log("User has less than 5 challenges with rank sub 200!")
        all_challenges.forEach(item => {
          if (challenges_1.indexOf(item) < 0) {
            // console.log("Missing challenge " + item)
            challenges_1.splice(all_challenges.indexOf(item), 0, item);
            ranks_1.splice(all_challenges.indexOf(item), 0, 200);
            scores_1.splice(all_challenges.indexOf(item), 0, 0);
          }
        })
      }
    });
}

async function getchartData_2 () {

  await fetch(`https://cultris.net:5001/leaderboard_stats_by_id/` + userID_2)
    .then(response => response.json())
    .then(data2 => {
      playerChallengeData_2 = data2
      console.log(playerChallengeData_2)

      all_challenges.forEach( item => {
        let cn = item
        playerChallengeData_2.forEach (entry => {
          if (entry.challengename == cn) {
            challenges_2.splice(all_challenges.indexOf(cn), 0, cn)
            ranks_2.splice(all_challenges.indexOf(cn), 0, entry.rank)
            scores_2.splice(all_challenges.indexOf(cn), 0, entry.score)
          }
        })
      })

      if (challenges_2.length < 5) {
        // console.log("User has less than 5 challenges with rank sub 200!")
        all_challenges.forEach(item => {
          if (challenges_2.indexOf(item) < 0) {
            // console.log("Missing challenge " + item)
            challenges_2.push(item)
            challenges_2.splice(all_challenges.indexOf(item), 0, item);
            ranks_2.splice(all_challenges.indexOf(item), 0, 200);
            scores_2.splice(all_challenges.indexOf(item), 0, 0);
          }

        })
      }
    });
}


// Chart
var challengesCanvas = document.getElementById("challengesChart");

function createChallengesChart() {

  getchartData_1().then(data => {
    getchartData_2().then(data => {
      let all_challenges_labels = ["Maserati", "Fortnight", "Cheese", "Ten", "Survivor"]

      var challengesData = {
        labels: all_challenges_labels,
        datasets: [{
          label: playerData_1.name,
          backgroundColor: "rgba(200,0,0,0.2)",
          data: ranks_1
        },
        {
          label: playerData_2.name,
          backgroundColor: "rgba(0,0,200,0.2)",
          data: ranks_2
        }]};

      var radarChart = new Chart(challengesCanvas, {
        type: 'radar',
        data: challengesData,
        options: options
      });
    })
  })

  }
  getplayerData(userID_1).then(data => {
    playerData_1 = data
    getplayerData(userID_2).then(data => {
      playerData_2 = data
      createChallengesChart()
      getTotalPlayers().then(data => {
        totalPlayers = data;
        loadplayersData(playerData_1, playerData_2)
      });
    });
  });
