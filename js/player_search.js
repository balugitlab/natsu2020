var allPlayers = []

async function getPlayers() {
  let url = `https://cultris.net:5000/full_rankings/`;
  const response = await fetch(url);
  const data = await response.json();
  return data
}

$( document ).ready(function() {
  var playerList = document.getElementById('playerList');

  getPlayers().then(data => {
    data.forEach(item => {
      allPlayers.push(item);
      var optionElement = document.createElement("option");
    })
  })

  document.getElementById('searchBar').addEventListener('keyup', function (event) {
    if (document.getElementById("searchBar").value.length < 2) {
      return;
    }
    playerList.textContent = '';
    for (var i = 0; i < allPlayers.length; i++) {
      search = allPlayers[i].Name;
      if (search.indexOf(document.getElementById("searchBar").value) !== 0) {
          continue;
      }
      var optionElement = document.createElement("option");
      optionElement.value = allPlayers[i].Name;
      playerList.appendChild(optionElement);
    }
  })
})

function enterHelper(e) {
  if (e.keyCode === 13) {
    // Cancel the default action, if needed
    e.preventDefault();
    console.log("ENTER DETECTED")
    playerLoad()
  }
}

function playerGo(playerID) {
  window.location.href = "index.html?player=" + playerID;
  return
}

async function searchUserId(name) {
  let url = "https://cultris.net:5000/player_by_name/" + name
  console.log(url)
  const response = await fetch(url)
  const data = await response.json()
  return data[0].UserId
}

function playerLoad() {
  if (document.getElementById("searchBar").value.length < 1) {
    return;
  }
  searchUserId(document.getElementById("searchBar").value).then(data => {
    playerGo(data)
  });
}

function playerLoadMobile() {
  if (document.getElementById("searchBarMobile").value.length < 1) {
    return;
  }
  searchUserId(document.getElementById("searchBarMobile").value).then(data => {
    playerGo(data)
  });
}

function enterHelperMobile(e) {
  if (e.keyCode === 13) {
    // Cancel the default action, if needed
    e.preventDefault();
    console.log("ENTER DETECTED")
    playerLoadMobile()
  }
}


function playerCompare() {
  if (document.getElementById("searchBar").value.length < 1) {
    return;
  }
  searchUserId(document.getElementById("searchBar").value).then(data => {
    if (window.location.href.indexOf("comparison") > -1) {
      window.location.href = "comparison.html?player1=" + userID_1 + "&player2=" + data;
      return;
    }

    window.location.href = "comparison.html?player1=" + userID + "&player2=" + data;
    return;
  });
}
