// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

function number_format(number, decimals, dec_point, thousands_sep) {
  // *     example: number_format(1234.56, 2, ',', ' ');
  // *     return: '1 234,56'
  number = (number + '').replace(',', '').replace(' ', '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

// Chart
var ctx = document.getElementById("rankChart");
var timestamps_1 = [];
var ranksData_1 = [];
var scoresData_1 = [];
var dailyData_1

var timestamps_2 = [];
var ranksData_2 = [];
var scoresData_2 = [];
var dailyData_2

async function getRankData_1() {
  await fetch(`https://cultris.net:5000/daily_stats_by_id/`+ userID_1)
    .then(response => response.json())
    .then(data => {
      dailyData_1 = data;
    });

    dailyData_1.forEach( entry => {
      timestamps_1.push(entry.timestamp.substring(0,5));
    });
    dailyData_1.forEach( entry => {
      ranksData_1.push(entry.rank);
    });
    document.getElementById("playerPeakRank_1").innerHTML = Math.min(...ranksData_1)
    dailyData_1.forEach( entry => {
      scoresData_1.push(entry.score);
    });
  }

async function getRankData_2() {
  await fetch(`https://cultris.net:5000/daily_stats_by_id/`+ userID_2)
    .then(response => response.json())
    .then(data => {
      dailyData_2 = data;
    });

    dailyData_2.forEach( entry => {
      timestamps_2.push(entry.timestamp.substring(0,5));
    });
    dailyData_2.forEach( entry => {
      ranksData_2.push(entry.rank);
    });
    document.getElementById("playerPeakRank_2").innerHTML = Math.min(...ranksData_2)
    dailyData_2.forEach( entry => {
      scoresData_2.push(entry.score);
    });
  }

function createPlayerChart() {

    getRankData_1().then(data => {
      getRankData_2().then(data => {
        // console.log(timestamps_1)
        // console.log(timestamps_2)
        //
        // console.log(ranksData_1)
        // console.log(ranksData_2)
        //
        //
        // console.log(scoresData_1)
        // console.log(scoresData_2)

        var myLineChart = new Chart(ctx, {
          type: 'line',
          data: {
            labels: timestamps_1,
            datasets: [{
              label:  dailyData_2[0].name + " Rank",
              yAxisID: 'A',
              fill: false,
              lineTension: 0.3,
              backgroundColor: "rgba(78, 115, 223, 0.05)",
              borderColor: "rgba(78, 115, 223, 1)",
              pointRadius: 3,
              pointBackgroundColor: "rgba(78, 115, 223, 1)",
              pointBorderColor: "rgba(78, 115, 223, 1)",
              pointHoverRadius: 3,
              pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
              pointHoverBorderColor: "rgba(78, 115, 223, 1)",
              pointHitRadius: 10,
              pointBorderWidth: 2,
              data: ranksData_2
            },
            {
              label: dailyData_2[0].name + " Points",
              yAxisID: 'B',
              fill: false,
              lineTension: 0.3,
              backgroundColor: "rgba(78, 115, 223, 0.05)",
              borderColor: "rgba(78, 115, 223, 0.05)",
              pointRadius: 3,
              pointBackgroundColor: "rgba(78, 115, 223, 0.05)",
              pointBorderColor: "rgba(78, 115, 223, 0.05)",
              pointHoverRadius: 3,
              pointHoverBackgroundColor: "rgba(78, 115, 223, 0.05)",
              pointHoverBorderColor: "rgba(78, 115, 223, 0.05)",
              pointHitRadius: 10,
              pointBorderWidth: 2,
              data: scoresData_2
            },
            {
              label:  dailyData_1[0].name + " Rank",
              yAxisID: 'A',
              fill: false,
              lineTension: 0.3,
              backgroundColor: "rgba(255, 56, 83, 0.05)",
              borderColor: "rgba(255, 56, 83, 1)",
              pointRadius: 3,
              pointBackgroundColor: "rgba(255, 56, 83, 1)",
              pointBorderColor: "rgba(255, 56, 83, 1)",
              pointHoverRadius: 3,
              pointHoverBackgroundColor: "rgba(255, 56, 83, 1)",
              pointHoverBorderColor: "rgba(255, 56, 83, 1)",
              pointHitRadius: 10,
              pointBorderWidth: 2,
              data: ranksData_1
            },
            {
              label: dailyData_1[0].name + " Points",
              yAxisID: 'B',
              fill: false,
              lineTension: 0.3,
              backgroundColor: "rgba(255, 56, 83, 0.05)",
              borderColor: "rgba(255, 56, 83, 0.05)",
              pointRadius: 3,
              pointBackgroundColor: "rgba(255, 56, 83, 0.05)",
              pointBorderColor: "rrgba(255, 56, 83, 0.05)",
              pointHoverRadius: 3,
              pointHoverBackgroundColor: "rgba(255, 56, 83, 0.05)",
              pointHoverBorderColor: "rgba(255, 56, 83, 0.05)",
              pointHitRadius: 10,
              pointBorderWidth: 2,
              data: scoresData_1
            }],
          },
          options: {
            maintainAspectRatio: false,
            layout: {
              padding: {
                left: 10,
                right: 25,
                top: 25,
                bottom: 0
              }
            },
            scales: {
              xAxes: [{
                time: {
                  unit: 'date'
                },
                gridLines: {
                  display: false,
                  drawBorder: false
                },
                ticks: {
                  maxTicksLimit: 15
                }
              }],
              yAxes: [{
                id: 'A',
                position: 'left',
                scaleLabel: {
                  display: true,
                  labelString: 'Rank'
                },
                ticks: {
                  maxTicksLimit: 5,
                  reverse: true,
                  padding: 10,
                  suggestedMin: 1,
                  // Include a dollar sign in the ticks
                  callback: function(value, index, values) {
                    return number_format(value);
                  }
                },
                gridLines: {
                  color: "rgb(234, 236, 244)",
                  zeroLineColor: "rgb(234, 236, 244)",
                  drawBorder: false,
                  borderDash: [2],
                  zeroLineBorderDash: [2]
                }
              },
              {
                id: 'B',
                position: 'right',
                scaleLabel: {
                  display: true,
                  labelString: 'Points'
                },
                ticks: {
                  maxTicksLimit: 5,
                  reverse: false,
                  padding: 10,
                  // suggestedMax: 500,
                  // Include a dollar sign in the ticks
                  callback: function(value, index, values) {
                    return number_format(value);
                  }
                },
              }],
            },
            legend: {
              display: true,
              position: 'bottom',
            },
            tooltips: {
              backgroundColor: "rgb(255,255,255)",
              bodyFontColor: "#858796",
              titleMarginBottom: 10,
              titleFontColor: '#6e707e',
              titleFontSize: 14,
              borderColor: '#dddfeb',
              borderWidth: 1,
              xPadding: 15,
              yPadding: 15,
              displayColors: false,
              intersect: false,
              mode: 'index',
              caretPadding: 10,
              callbacks: {
                label: function(tooltipItem, chart) {
                  var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
                  return datasetLabel + ':' + number_format(tooltipItem.yLabel);
                }
              }
            }
          }
        });


      })
    })
  };

createPlayerChart()
