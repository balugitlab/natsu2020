function loadTableData(items) {
  const table = document.getElementById("dataTable");
  items.forEach( item => {
    let row = table.insertRow();
    let challenge = row.insertCell(0);
    challenge.innerHTML = item.challengename.charAt(0).toUpperCase() + item.challengename.slice(1);
    let rank = row.insertCell(1);
    rank.innerHTML = item.rank;
    let score = row.insertCell(2);
    score.innerHTML = item.score;
  });
}

function loadplayersData(playerData_1, playerData_2) {
  document.getElementById("playerName_1").innerHTML = playerData_1.name
  document.getElementById("playerRank_1").innerHTML = playerData_1.rank + " of " + totalPlayers
  document.getElementById("playerWeekPlayTime_1").innerHTML = Math.round(playerData_1.WeekPlaytime / 60) + " Hours"
  document.getElementById("playerNetScore_1").innerHTML = playerData_1.netscore + " Points"
  document.getElementById("playerStatsName_1").innerHTML = playerData_1.name
  document.getElementById("playerTimePlayed_1").innerHTML = "Total Hours Played: " + Math.round(playerData_1.playedmin/60)
  document.getElementById("playerGamesPlayed_1").innerHTML = "Total Games Played: " + Math.round(playerData_1.playedrounds)
  document.getElementById("playerWinRate_1").innerHTML = "Win Rate: " + Math.round(100*(playerData_1.wins) / (playerData_1.playedrounds)) + "%"
  document.getElementById("playerAvgBPM_1").innerHTML = "Average Speed: " + Math.round(playerData_1.avgbpm) + " BPM"
  document.getElementById("playerMaxBPM_1").innerHTML = "Max Speed: " + Math.round(playerData_1.maxbpm) + " BPM"
  document.getElementById("playerTimeStamp_1").innerHTML = "Last Updated: " + playerData_1.timestamp.substring(0,10)




  document.getElementById("playerName_2").innerHTML = playerData_2.name
  document.getElementById("playerRank_2").innerHTML = playerData_2.rank + " of " + totalPlayers
  document.getElementById("playerWeekPlayTime_2").innerHTML = Math.round(playerData_2.WeekPlaytime / 60) + " Hours"
  document.getElementById("playerNetScore_2").innerHTML = playerData_2.netscore + " Points"
  document.getElementById("playerStatsName_2").innerHTML = playerData_2.name
  document.getElementById("playerTimePlayed_2").innerHTML = "Total Hours Played: " + Math.round(playerData_2.playedmin/60)
  document.getElementById("playerGamesPlayed_2").innerHTML = "Total Games Played: " + Math.round(playerData_2.playedrounds)
  document.getElementById("playerWinRate_2").innerHTML = "Win Rate: " + Math.round(100*(playerData_2.wins) / (playerData_2.playedrounds)) + "%"
  document.getElementById("playerAvgBPM_2").innerHTML = "Average Speed: " + Math.round(playerData_2.avgbpm) + " BPM"
  document.getElementById("playerMaxBPM_2").innerHTML = "Max Speed: " + Math.round(playerData_2.maxbpm) + " BPM"
  document.getElementById("playerTimeStamp_2").innerHTML = "Last Updated: " + playerData_2.timestamp.substring(0,10)
}

async function getplayerChallengeData(id) {
  const response = await fetch('https://cultris.net:5001/leaderboard_stats_by_id/' + id)
  const data = await response.json()
  return data
}

async function getTotalPlayers() {
  const response = await fetch('https://cultris.net:5000/full_rankings/');
  const data = await response.json();
  return data.length;
}

async function getplayerData(id) {
  const response = await fetch(`https://cultris.net:5000/current_stats_by_id/` + id);
  const data = await response.json();
  return data
}

// getplayerChallengeData(userID_1).then(data => {
//   playerChallengeData_1 = data;
//   // loadTableData(playerChallengeData_1)
// });
// getplayerChallengeData(userID_2).then(data => {
//   playerChallengeData_2 = data;
//   // loadTableData(playerChallengeData_2)
// });

getplayerData(userID_1).then(data => {
  playerData_1 = data
  getplayerData(userID_2).then(data => {
    playerData_2 = data
    getTotalPlayers().then(data => {
      totalPlayers = data;
      loadplayersData(playerData_1, playerData_2)
    });
  });
});
