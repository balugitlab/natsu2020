var leaderboard = document.getElementById("leaderboard").getAttribute("data-ID");
document.getElementById("leaderboardName").innerHTML = leaderboard.charAt(0).toUpperCase() + leaderboard.slice(1) + " Rankings";

function find_in_object(my_object, my_criteria){

  return my_object.filter(function(obj) {
    return Object.keys(my_criteria).every(function(c) {
      return obj[c] == my_criteria[c];
    });
  });

}

$('#leaderboardTable').on('click', 'tbody > tr > td', function (e) {
    // 'this' refers to the current <td>, if you need information out of it
    e.preventDefault();
    var table = $('#leaderboardTable').DataTable();
    var data = table.row($(this).closest('tr')).data();
    var id = data.userid;
    console.log(data);
    if (id === undefined) {
      console.log("id is undefined!!");
      // console.log(data.Name)
      url = "https://cultris.net:5000/player_by_name/" + data.Name;
      console.log(url);
      async function getUserId(){
        const response = await fetch(url);
        const data = await response.json();
        return data;
      }
      getUserId().then(data => {
        id = data[0].UserId;
        console.log(id);
        if (id !== undefined) {
          window.location.href = "index.html?player=" + id;
          return
        }
      })
    }
    if (id !== undefined) {
      window.location.href = "index.html?player=" + id;
      return
    }

  });

switch (leaderboard) {
  case "maserati":
    url = `https://cultris.net:5001/leaderboard/` + leaderboard;
    document.getElementById("leaderboardDesc").innerHTML = "Clear 40 lines";
    columns = [ {
      "data" : "rank"
    }, {
      "data" : "name"
    }, {
      title: "Time",
      "data" : "score"
    }]
    $(document).ready(function() {
      $('#leaderboardTable').DataTable( {
        "processing": true,
        "ajax" : {
          "url" : url,
          dataSrc : ''
        },
        "columns" : columns
      });
    });
    break;
  case "survivor":
    url = `https://cultris.net:5001/leaderboard/` + leaderboard;
    document.getElementById("leaderboardDesc").innerHTML = "Play as long as you can"
    columns = [ {
      "data" : "rank"
    }, {
      "data" : "name"
    }, {
      title: "Time",
      "data" : "score"
    }]
    $(document).ready(function() {
      $('#leaderboardTable').DataTable( {
        "processing": true,
        "ajax" : {
          "url" : url,
          dataSrc : ''
        },
        "columns" : columns
      });
    });
    break;
  case "fortnight":
    url = `https://cultris.net:5001/leaderboard/` + leaderboard;
    document.getElementById("leaderboardDesc").innerHTML = "Send as many lines as you can in a minute"
    columns = [ {
      "data" : "rank"
    }, {
      "data" : "name"
    }, {
      title: "Lines Sent",
      "data" : "score"
    }]
    $(document).ready(function() {
      $('#leaderboardTable').DataTable( {
        "processing": true,
        "ajax" : {
          "url" : url,
          dataSrc : ''
        },
        "columns" : columns
      });
    });
    break;
  case "ten":
    url = `https://cultris.net:5001/leaderboard/` + leaderboard;
    document.getElementById("leaderboardDesc").innerHTML = "Do a level 10 combo"
    columns = [ {
      "data" : "rank"
    }, {
      "data" : "name"
    }, {
      title: "Time",
      "data" : "score"
    }]
    $(document).ready(function() {
      $('#leaderboardTable').DataTable( {
        "processing": true,
        "ajax" : {
          "url" : url,
          dataSrc : ''
        },
        "columns" : columns
      });
    });
    break;
  case "cheese":
    url = `https://cultris.net:5001/leaderboard/` + leaderboard;
    document.getElementById("leaderboardDesc").innerHTML = "Complete a swiss cheese challenge"
    columns = [ {
      "data" : "rank"
    }, {
      "data" : "name"
    }, {
      title: "Time",
      "data" : "score"
    }]
    $(document).ready(function() {
      $('#leaderboardTable').DataTable( {
        "processing": true,
        "ajax" : {
          "url" : url,
          dataSrc : ''
        },
        "columns" : columns
      });
    });
    break;
  case "FFA":
    url = "https://cultris.net:5000/full_rankings/";
    document.getElementById("leaderboardDesc").innerHTML = "Free-for-all rankings"
    $(document).ready(function() {
      $('#leaderboardTable').DataTable( {
        "processing": true,
        "ajax" : {
          "url" : url,
          dataSrc : function (json) {
            var return_data = new Array();
            for(var i=0;i< json.length; i++) {
              return_data.push({
                'Rank': json[i].Rank,
                'Name': json[i].Name,
                'Score': json[i].Score.toFixed(2)
              })
            }
            console.table(return_data)
            return return_data;
          }
        },
        "columns" : [ {
          "data" : "Rank"
        }, {
          "data" : "Name"
        }, {
          title: "Points",
          "data" : "Score"
        }]
      });
    });
    break;
  case "Netscores":
    url = "https://cultris.net:5000/netscores_rankings/";
    document.getElementById("leaderboardDesc").innerHTML = "Points gained or lost in FFA over the past 7 days"
    $(document).ready(function() {
      $('#leaderboardTable').DataTable( {
        "processing": true,
        "ajax" : {
          "url" : url,
          dataSrc : function (json) {
            var return_data = new Array();
            for(var i=0;i< json.length; i++) {
              return_data.push({
                'Rank': i+1,
                'Name': json[i].Name,
                'NetScore': json[i].NetScore.toFixed(2)
              })
            }
            console.table(return_data)
            return return_data;
          }
        },
        "columns" : [ {
          "data" : "Rank"
        }, {
          "data" : "Name"
        }, {
           title: "Net Points (last 7 days)",
          "data" : "NetScore"
        }]
      });
    });

    break;
  case "Activity":
    url = "https://cultris.net:5000/full_active_rankings/";
    document.getElementById("leaderboardDesc").innerHTML = "Time spent playing multiplayer over the past 7 days"
    $(document).ready(function() {
      $('#leaderboardTable').DataTable( {
        "processing": true,
        "ajax" : {
          "url" : url,
          dataSrc : function (json) {
            var return_data = new Array();
            for(var i=0;i< json.length; i++) {
              return_data.push({
                'Rank': i+1,
                'Name': json[i].Name,
                'WeekPlaytime': (json[i].WeekPlaytime/60).toFixed(2) + " hours"
              })
            }
            console.table(return_data)
            return return_data;
          }
        },
        "columns" : [ {
          "data" : "Rank"
        }, {
          "data" : "Name"
        }, {
          title: "Play time (last 7 days)",
          "data" : "WeekPlaytime"
        }]
      });
    });
    break;
    case "online":
      proxyurl = "https://cors-anywhere.herokuapp.com/";
      url = "https://gewaltig.net/liveinfo.aspx";
      var players = []
      document.getElementById("leaderboardDesc").innerHTML = "Online Players"
      $(document).ready(function() {
        $('#leaderboardTable').DataTable( {
          "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
          "processing": true,
          "ajax" : {
            "url" : proxyurl + url,
            dataSrc : function (json) {
              console.log(json)
              var return_data = new Array();
              var rooms = JSON.stringify(json.rooms);

              for(var i=0;i< json.players.length; i++) {
                var filtered_json = find_in_object(JSON.parse(rooms), {id: json.players[i].room});

                return_data.push({
                  'Name': json.players[i].name,
                  'Room': filtered_json[0].name,
                  'Country': json.players[i].country,
                  'Current Score': json.players[i].currentscore
                })
              }
              console.table(return_data);
              return return_data;
            }
          },
          "columns" : [ {
            title: "Room",
            "data" : "Room"
          }, {
          //   title: "Country",
          //   "data" : "Country"
          // }, {
            title: "Name",
            "data" : "Name"
          }, {
            title: "Current Score",
            "data" : "Current Score"
          }]
        });
      });
      break;
    }
